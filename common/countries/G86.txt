graphical_culture = equestriangfx

color = { 237 254  107 }

revolutionary_colors = { 12  13  8 }

historical_idea_groups = {
	administrative_ideas
	offensive_ideas
	maritime_ideas
	quality_ideas
	innovativeness_ideas
	defensive_ideas
	expansion_ideas
	trade_ideas
}

historical_units = {
	western_medieval_infantry
	western_medieval_knights
	western_men_at_arms
	dutch_maurician
	french_caracolle
	swedish_gallop
	swedish_gustavian
	swedish_caroline
	swedish_arme_blanche
	prussian_uhlan
	mixed_order_infantry
	open_order_cavalry
	napoleonic_square
	napoleonic_lancers
}

monarch_names = {
	"Gallus #0" = 10
	"Garner #0" = 10
	"Gaston #0" = 10
	"Gerard #0" = 10
	"Graff #0" = 10
	"Girwin #0" = 10
	"Grance #0" = 10
	"Garie #0" = 10
	"Gerath #0" = 10
	"Gronzechase #0" = 10
	"Geath #0" = 10
	"Gideon #0" = 10
	"Gripoil #0" = 10

	
	"Gamille #0" = -10
	"Gretchen #0" = -5
	"Gashley #0" = -1
	"Griselda #0" = -1
	"Gaia #0" = -1
	"Grayfille #0" = -1
	"Galsace #0" = -1
	"Gorraine #0" = -1
	"Gaterina #0" = -1
	"Gala #0" = -10
}

leader_names = {}

ship_names = {}

army_names = {}

fleet_names = {}

