graphical_culture = equestriangfx
color = { 99 181 176 }
revolutionary_colors = { 13  8  13 }
preferred_religion = harmonism

historical_idea_groups = {}

historical_units = {}

monarch_names = {
	"Karl #7" = 100

	"Ulrika Eleonora #0" = -10
}

leader_names = {
	Andersson Armfeldt "von Ascheberg" Adlercreutz Adlersparre Alkmaar Anckarström
	Bagge Banér Bielke Brahe
	Cronstedt
	"von Döbeln" Dahlbergh Dufwa
	Engelbrekt Eriksson Ehrensvärd "von Essen" Eka Ehrenstrahl
	Fleming "von Fersen"
 	Gadh Grip "af Gennäs" "de la Gardie" Gyllenhielm Gathenhielm Gustavsson
	Horn Hård Hahn Hastfer Hjärne
	Jägerhorn
	Königsmarck Klingspor
	Lewenhaupt Leijonhufvud Liljencrantz Lindschöld
	Oxenstierna Lilliehöök
	Palmstruch Piper Prinz
	Rehnskiöld Ribbing
	Stenbock Sandels Skytte Sture
	"Tre Rosor" Torstensson Trolle Toll Tott
	Wrangel Wachtmeister Wallenberg
	Åkerhielm
}

ship_names = {
	""
}

army_names = {
	"Army of"
}

fleet_names = {
	"Högsjöflottan" "Skärgårdsflottan"
}
