equestrian_princedom_reform = {	
	icon = "canterlot"
	allow_normal_conversion = no	
	allow_convert = no	
	lock_level_when_selected = yes	
	valid_for_new_country = no	
	valid_for_nation_designer = yes	
	rulers_can_be_generals = no	
	potential = {
		OR = {
			tag = CAN	
			was_tag = CAN	
			has_reform = equestrian_princedom_reform	
			have_had_reform = equestrian_princedom_reform	
		}
	}
	nation_designer_cost = 0
	modifiers = {
		liberty_desire_from_subject_development = -0.33
		nobles_influence_modifier = 0.05
		maratha_exclusive_influence_modifier = 0.05
		rajput_exclusive_influence_modifier = 0.05
	}
}