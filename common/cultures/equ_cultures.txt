#Equestrian cultures
equestrian = {
	#(some) names provided by https://www.fantasynamegenerators.com/my-little-pony-names.php
	male_names = {
		"Comet" "Spark" "Midnight" "Cobalt" "Solar" "Shining" "Astral" "Maverick"  "Arctic Breeze"  "Astral Thunder"  "Berry White"  "Blaze"  "Bouncer"  "Brisk Bronco"  
		"Bulky Buster"  "Caramel Crunch"  "Cloud Sweeper"  "Cobalt"  "Colt Feet"  "Colt Ice"  "Comet Flash"  "Crimson Mask"  "Crimson Moon"  "Crimson Vision"  "Dapper Dash"  
		"Dapper Force"  "Dark Meadow"  "Dark Snow"  "Dark Specter"  "Duke Bristle"  "Duke Starlight"  "Duke Venture"  "Emerald Mask"  "Fancy Hooves"  "Golden Flash"  
		"Ivory Spark"  "Jackpot Star"  "Jade Jester"  "Last Legacy"  "Little Snow"  "Lost Legacy"  "Marble Mark"  "Master Facade"  "Master Metal"  "Maverick"  "Midnight Hero"  
		"Mister Bristle"  "Mister Dare"  "Moonlight Hunter"  "Moonshadow Colt"  "Moonshadow Sprint"  "Moonstone Mustang"  "Mythic Haze"  "Night Twister"  "Nimble Force"  "Onyx"  
		"Onyx Armor"  "Onyx Bolt"  "Onyx Justice"  "Onyx Star"  "Platinum Night"  "Prince Prickle"  "Raggedy Randy"  "Rapid Shadow"  "Rocky Road"  "Sapphire Spirit"  
		"Sapphire Twister"  "Shadow Mark"  "Shining Star"  "Silver Mane"  "Silver Shine"  "Silver Spirit"  "Silver Whiskers"  "Sky Chaser"  "Sky Scraper"  "Smoky"  "Soaring Surfer"  
		"Solar Comet"  "Star Chaser"  "Star Whistle"  "Steel Mustang"  "Steel Stud"  "Straight Arrow"  "Sundance"  "Sunrise Storm"  "Sweet Sorbet"  "Swift Bolt"  "Tango"  
		"Thunder Bolt"  "Thunder Charge"  "Thunder Colt"  "Thunder Spark"  "Thunder Tail"  "Thunder Wing"  "Thunderhoof"  "Tiny Thunder"  "Twilight Thunder"  "Velvet Chaser"  
		"Wild Ace"  "Wild Strikes"  "Winter Gust"  "Yellow Rock"
	}
	female_names = {
		"Blueberry" "Twinkletoes" "Starfish" "Nettlekiss" "Hazelblossom" "Dazzleflash" "Buttercup" "Bubblegum" "Cherry Blossom" "Snowflake" "Water Lilly" "Twinkle Star" 
		"Saffron" "Pumpkin" "Cinnamon Twirl" "Sweetie Pie" "Cutie Pie" "Celeste" "Rose Petal" "Sugar Song" "Honey Bee" "Scarlet" "Obsidia" "Lollipop" "Sparkle" "Dahlia" 
		"Lucy Light" "Lavender" "Orchid Jewel" "Sapphire" "Sapphire Swing" "Scarlet Harmony" "Sapphire Sunlight" "Lunar Candy" "Midnight Harmony" "Violet Glow" "Velvet Love" 
		"Sapphire Daisy" "Fluffy Candy" "Fluffy Star" "Rainbow Dawn" "Pearl Petunia" "Caramel Candy" "Velvet Kisses" "Ruby Aurora" "Dew Drop" "Solar Kisses" "Ivy Jewel" 
		"Emerald Aura" "Violet Sparkle" "Violet Rain" "Strawberry Fashion" "Ivory Fire" "Shadow Flower" "Delilah Dusk" "Velvet Cupcake" "Star Eyes" "Celestial Song" 
		"Celestial Snowflake" "Snowy Blossom" "Azure Moon" "Azure Shadow" "Mythic Diamond" "Sapphire Flower" "Lunar Flower" "Sapphire Moonlight" "Silverfoot" "Aqua Lilly" 
		"Lillypad Love" "Lila Love" "Emerald Snow" "Sugar Spice" "Chocolate Harmony" "Electric Harmony" "Ebony Moon" "River Breeze" "Ebony Breeze" "Crystal Rose" 
		"Diamond Blossom" "Ice Blossom" "Phantasia" "Starry Night" "Moon Petal" "Emerald Dream" "Sandy Shadow" "Ivory Charm" "Lucky Star" "Lucky Lucy" "Ocean Breeze" 
		"Cherry Berry" "Caramel Smooch" "Caramel Kisses" "Amethyst Rose" "Lunar Love" "Scarlet Shadow" "Mythic Fashion" "Little Harmony" "Little Snowflake" "Starry Diamond" 
		"Starry Swirl" "Nightlight Nourish" "Honey Cake" "Amber Night" "Amber Gem" "Flawless Gem" "Twilight Snowflake" "Fluffy Wings"
	}
	dynasty_names = {
		"Hoofler" "Goldenflash" "Solarbrisk" "Crimsonmoon" "Silvermane"  "Blueberry" "Twinkletoes" "Starfish" "Nettlekiss" "Hazelblossom" "Dazzleflash" "Dancing Star" 
		"Dark Vision" "Frostwing" "Frostblossom" "Frozenhoof" "Fruitcake" "Goldenmoon" "Moondance" "Moondust" "Moonshadow" "Morning Haste" "Mystic Manes" "Mythic Wish" 
		"Ocean Breeze" "Shadowcharm" "Shadowfang" "Shadowhoof" "Shiningstar" "Shootingstar" "Shufflestep" "Silentstep" "Silvermanes" "Silvershadow" "Silversong" "Silverstream" 
		"Skystar" "Sneezy" "Snowstar" "Star Eyes" "Starburst" "Stonesteps" "Sugarcube" "Sugardash" "Swift Mist" "Winternight" "Wintersong" "Lulamoon"
	}

	upper_ponish = { primary = "CAN" }
	
	lower_ponish = {}

	neighderlander = { primary = "NEI" }

	crystalian = {
		primary = "CRY"
	}

	stuten = {}

	lovanian = {
		primary = "LOV"
		dynasty_names = {
			"von Kuzich" "von Mond" "von Zon" "von Pferdenberg" "von Satteln"
		}
	}

	trottenian = {}

	nefosian = {}

	cloppenian = {}

	thalassian = {}

	rivenian = {}
}

chevalian = {

	male_names = {
		"Aile du Tonnerre" "Armure Brillante" "Astral" "Baie Blanche" "Balayeuse de Nuages" "Boulon" "Brillant" "Brise Arctique" "Bronco Vif" "Brume Mythique" "Buster Volumineux" "Charge de Tonnerre" "Chasseur d'Etoiles" "Chasseur de Velours" "Chasseur du Ciel" "Chasseur" "Chemin Rocailleux" "Cobalt" "Comete Solaire" "Cometo" "Coup de Tonnerre" "Criniere d'Argent" "Croustillant Au Caramel" "Danse du Soleil" "Duc Starlight" "Duc Venture" "Eclair Comete" "Eclair Dore" "Eclair Rapide" "Eclat d'Argent" "Enfume" "Esprit Saphir" "Esprit d'Argent" "Etincelle Ivoire" "Etincelle de Tonnerre" "Etincelle" "Etoile Brillante" "Etoile Onyx" "Facade Principale" "Flamber" "Fleche Droite" "Force Agile" "Force Pimpante" "Frappes Sauvages" "Goujon en Acier" "Gratteciel" "Heritage Perdu" "Heros de Minuit" "Justice Onyx" "Lune Cramoisie" "Marque de L'ombre" "Marque de Marbre" "Masque Cramoisi" "Masque d'Emeraude" "Maverick" "Ma�tre Metal" "Minuit" "Monsieur Bristle" "Monsieur Dare" "Moustaches d'Argent" "Mustang d'Acier" "Mustango" "Neige Sombre" "Nuit Platine" "Ombre Rapide" "Petit Tonnerre" "Petite Neige" "Pieds de Poulain" "Poils Duc" "Poulain Glace" "Poulain du Tonnerre" "Prairie Sombre" "Prince Piquant" "Queue de Tonnerre" "Rafale d'Hiver" "Raggedy Randy" "Roche Jaune" "Sabots Fantaisie" "Sauvage" "Sifflet Etoile" "Solaire" "Sorbet Sucre"
	}

	female_names = {
		"Diamanta" "Myrtille" "Etoile de Mer" "Ortie" "Noisetier" "Eblouissement" "Renoncule" "Chicle" "Fleur de Cerisier" "Flocon de Neige" "Nenuphar" "Etoile Scintillante" "Safran" "Citrouille" "Tourbillon de Cannelle" "Sweetie Pie" "Tarte Mignonne" "Celeste" "Petale de Rose" "Chanson de Sucre" "Abeille" "Ecarlate" "Obsidienne" "Sucette" "Scintillait" "Dahlia" "Lucie Lumiere" "Lavande" "Bijou Orchidee" "Saphir" "Balan�oire Saphir" "Harmonie Ecarlate" "Lumiere du Soleil Saphir" "Bonbon Lunaire" "Harmonie de Minuit" "Lueur Violette" "Amour de Velours" "Marguerite Saphir" "Bonbons Moelleux" "Etoile Moelleuse" "Aube Arcoiris" "Perle Petunia" "Bonbons Au Caramel" "Baisers de Velours" "Rubis Aurore" "Goutte de Rosee" "Baisers Solaires" "Bijou de Lierre" "Aura d'Emeraude" "Eclat Violet" "Pluie Violette" "Mode Fraise" "Feu d'Ivoire" "Fleur d'Ombre" "Dalila Crepuscule" "Cupcake Velours" "Yeux Etoiles" "Chant Celeste" "Flocon de Neige Celeste" "Fleur Enneigee" "Lune Azur" "Ombre Azur" "Diamant Mythique" "Fleur de Saphir" "Fleur Lunaire" "Clair de Lune Saphir" "Silverfoot" "Aqua Lilly" "Lillypad Amour" "Lila Amour" "Neige Emeraude" "Sucre Epice" "Harmonie Chocolat" "Harmonie Electrique" "Lune d'Ebene" "Brise de Riviere" "Brise d'Ebene" "Cristal Rose" "Fleur de Diamant" "Fleur de Glace" "Phantasia" "Nuit Etoilee" "Petale de Lune" "R�ve d'Emeraude" "Ombre de Sable" "Charme Ivoire" "Bonne Etoile" "Lucie Chanceuse" "Brise" "Cerise Baie" "Escarlata" "Obsidia" "Chupete" "Brillar" "Baise Au Caramel" "Bisous Au Caramel" "Amethyste Rose" "Amour Lunaire" "Ombre Ecarlate" "Mode Mythique" "Petite Harmonie" "Petit Flocon de Neige" "Diamant Etoile" "Tourbillon Etoile" "Nourrir La Veilleuse" "Gateau Au Miel" "Nuit Ambre" "Gemme Ambre" "Gemme Sans Defaut" "Flocon de Neige Crepusculaire" "Ailes Moelleuses"
	}
	
	dynasty_names = {
		"de Platine" "d'Bulione" "de Luna"
	}

	prancien = {
		primary = "PRA"
	}

	hevonian = {
		primary = "HEV"
	}

	zaldi = {
		primary = "PRA"
	}
	
	hufferian = {}

	cascien = {}

	ferran = {}

	praderan = {}

	asimian = {}
}

ipposic = {

	dynasty_names = {
		"Kometos"
	}

	male_names = { 
		"Komitis" "Spitha" "Mesanikhta" "Kovaltio" "Iliakos" "Lampsi" "Astrikos" "Anorthodoxos" "Beri Lefko" "Phloga" "Eftaxias Potopoliou" "Ongodes Buster" "Tragano Karamela" "Kovaltio" "Katakokkini Maska" "Katakokkini Selini" "Skotino Livadi" "Skotino Khioni" "Skotino Phantasma" "Smaragd" "Phantastikes Oples" "Khrisi Lampsi" "Tzakpot Asteri" "Jade Jester" "Teleftaia Klironomia" "Mikro Khioni" "Khameni Klironomia" "Marmaro" "Prosopsi" "Anorthodoxos" "Iroas Tou Mesonikhtiou" "Kirie Bristl" "Kirie Dre" "Kinigos" "Efkiniti Dinami" "Onikhas" "Bouloni Onikha" "Pringipas Prikl" "Dromos" "Sima Skias" "Lampro Asteri" "Asimenia Khaiti" "Asimenia Lampsi" "Asimenia Moustakia" "Ouranoxistis" "Kapnodis" "Serpher Sta Ipsi" "Iliakos Komitis" "Asteri Kinigos" "Sphirikhtra Asterion" "Atsalino" "Efthi Velos" "Kataiyida Anatolis" "Gliko Sormpe" "Swift Bolt" "Tango" "Agrios Asos" "Khimoniatiki Ripi"
	}

	female_names = {
		"Mirtilos" "Astrapi" "Asterias" "Tsouknida" "Phountouki" "Vatrakhio" "Tsikhlophouska" "Anthos Kerasias" "Niphada Khioniou" "Krokos" "Kolokithi" "Striphoyirisma Kanelas" "Glikia Pita" "Selest" "Petalo Triantaphillou" "Tragoudi Zakharis" "Melissa" "Kokkinos" "Opsidia" "Gliphitzouri" "Lampo" "Dalia" "Lousi La�t" "Levanta" "Orkhidea Kosmima" "Zaphiri" "Seliniaki Karamela" "Violeti Lampsi" "Veloudini Agapi" "Aphrati Karamela" "Khnoudoto Asteri" "Margaritari Petounia" "Karamela Karamela" "Veloudina Philia" "Stagona Drosias" "Iliaka Philia" "Smaragdenia Avra" "Violeti Vrokhi" "Moda Phraoula" "Skia Louloudi" "Veloudino" "Ouranio Tragoudi" "Ourania Niphada Khioniou" "Galazia Selini" "Louloudi Zaphiri" "Seliniako Louloudi" "Lila Agapi" "Smaragdenio Khioni" "Bakhariko Zakharis" "Sokolata Armonia" "Ilektriki Armonia" "Kristallino Triantaphillo" "Phantasia" "Enastri Nikhta" "Petalo Selinis" "Tikhero Asteri" "Tikheri Lousi" "Karamela Smooch" "Philia Karamelas" "Amethistos Roouz" "Seliniaki Agapi" "Mithiki Moda" "Mikri Armonia" "Mikri Niphada Khioniou" "Enastro Diamanti" "Nightlight Nourish" "Keik Me Meli" "Kekhrimpari Nikhta" "Apsogo Stolidi" "Likophos Niphada Khioniou" "Aphrata Phtera"
	}

	kelistran = {
		primary = "KEL"
	}
	alogian = {}

	stavlos = {}

	petalian = {}
}


neighic = { #pseudo-slavic
	kobylish = { primary = "KOB" }
	sedlan = {}
	koldik = {}
	kapytan = {}
	asiel = {}
	hryvan = {}
	premoranian = {}
	vyspian = {} #formable culture

	trotsylvanian = {
		primary = "TRO"
	}

	female_names = {
		"Ametisticzna Vrtnica" "Azurna Luna" "Azurna Senca" "Biserna Petunija" "Bleszczati" "Bleszczecz Blisk" "Borovnica" "Brezhiben Dragulj" "Brszljanov Dragulj" "Bucza" "Celesta" "Cimetov Vrtinec" "Cukrczka" "Czebela" "Czesznjev Cvet" "Czesznjeve Jagode" "Czokoladna Harmonija" "Dalija" "Delilah Mrak" "Diamantni Cvet" "Dragulj Orhideje" "Drzyploszka" "Ebenovina Luna" "Elektriczna Harmonija" "Fantazija" "Gvezdka" "Jagodna Moda" "Jantarna Nocz" "Jantarni Dragulj" "Kaplja Rosa" "Karamelni Bonboni" "Karamelni Poljubi" "Karamelni Smooch" "Kaszanka" "Kasztanka" "Kopriva" "Kristalna Vrtnica" "Ledeni Cvet" "Lesznikov Cvet" "Lizika" "Lunarna Ljubezen" "Lunarni Bonboni" "Lunin Cvet" "Mala Snezinka" "Malo Harmonije" "Maslenica" "Mavriczna Zora" "Medena Torta" "Mitska Moda" "Mitski Diamant" "Morskovezda" "Nebesna Snezinka" "Nebeszka Pesem" "Noczna Lucz Neguje" "Obsidia" "Oceanski Vetricz" "Ogenj Slonovine" "Peszczena Senca" "Polnoczna Harmonija" "Puhasta Krila" "Puhasta Zvezda" "Puhasti Bonboni" "Reczni Vetricz" "Rubinska Aurora" "Safir" "Safirjev Cvet" "Safirna Gugalnica" "Safirna Marjetica" "Safirna Meseczina" "Safirna Sonczna Svetloba" "Senczni Cvet" "Sivka" "Sladkorna Zaczimba" "Slonokoszczeni Czar" "Smaragdne Sanje" "Smaragdni Sneg" "Snezinka" "Snezka" "Snezni Cvet" "Somraczna Snezinka" "Sonczni Poljubi" "Srczek" "Srebrno Stopalo" "Sreczna Lucy" "Sreczna Zvezda" "Szkrlat" "Szkrlatna Harmonija" "Szkrlatna Senca" "Szmaragdna Aura" "Utripajocza Zvezda" "Utripajoczi" "Vetricz Iz Ebenovine" "Vijoliczna Iskrica" "Vijoliczni Dez" "Vijoliczni Sijaj" "Vodna Lilija" "Zafran" "Zametna Ljubezen" "Zametni Kolaczek" "Zametni Poljubi" "Zveczilni Gumi" "Zvezdnata Nocz" "Zvezdne Oczi" "Zvezdni Diamant" "Zvezdni Vrtinec" 
	}
	
	male_names = {
		
		"Beneda" "Benes" "Blus" "Bohuse" "Borek" "Bores" "Borut" "Bucefal" "Ceh" "Cerno" "Chezil" "Chval" "Cuk" "Dervan" "Divis" "Dobes" "Drago" "Drosuk" "Drslav" "Etgar" "Gavel" "Gojmir" "Golob" "Hasek" "Havel" "Henik" "Heralt" "Hoten" "Hrabise" "Hruta" "Hrvat" "Inko" "Ivko" "Jaros" "Jesek" "Jidrej" "Jimram" "Jiri" "Jirik" "Jurk" "Kadalhoh" "Kajtimar" "Karel" "Kolman" "Kornel" "Kosuta" "Kragel" "Kreslav" "Kruto" "Kuna" "Ljuban" "Matous" "Medved" "Metoslovas" "Milivoj" "Mojmir" "Negomir" "Ojir" "Orel" "Otakar" "Perun" "Petrus" "Prokop" "Radan" "Sambor" "Semil" "Snezek" "Snovid" "Svetko" "Svetomir" "Tvaruk" "Valto" "Vnislav" "Vojtich" "Volcic" "Vran" "Vsebor" "Komet" "Iskra" "Polnocz" "Kobalt" "Sonczna" "Sijaj" "Astral" "Maverick" "Arkticzni Vetricz" "Belojahod" "Velikpozar" "Odbojnik" "Zivahni Bronco" "Pometacz Oblakov" "Kobalt" "Zimnonoh" "Kometblysk" "Temnitravnik" "Temnisneg" "Smaragdna Maska" "Zlatiblisk" "Zvezda" "Zadnja Zapuszczina" "Mali Sneg" "Izgubljena Zapuszczina" "Marmorna Oznaka" "Master Fasada" "Master Metal" "Maveryk" "Polnoczni Junak" "Gospod" "Gospod Dare" "Mitska Meglica" "Noczni Twister" "Spretnasil" "Oniks" "Platinasta Nocz" "Princ Prickle" "Hitra Senca" "Kamnita Cesta" "Safirni Duh" "Safir Twister" "Senczna Oznaka" "Srebrna Griva" "Srebrni Sijaj" "Srebrni Duh" "Srebrni Brki" "Sky Chaser" "Nebo Strgalo" "Sonczni Komet" "Star Chaser" "Zvezdna Piszczalka" "Jekleni Mustang" "Jekleni Czep" "Ravna Puszczica" "Sonczni Vzhod" "Sladki Sorbet" "Tanho" "Gromoglavo Kopito" "Majhen Grom" "Twilight Thunder" "Velvet Chaser" "Divji As" "Zimski Sunek" "Rumena Skala"
	}
	
	dynasty_names = {
		"Borovkicz" "Bryzovicz" "Cichokrok" "Cienkielicz" "Cukrostek" "Czartenski" "Gvezdovicz" "Kamenski" "Kichajski" "Kopycki" "Kovalski" "Kredens" "Ksiujic" "Kucovicz" "Kucovski" "Kucys" "Kutasievicz" "Kvitnovski" "Mitozic" "Nebogvezd" "Orechkvit" "Piasenek" "Platynovicz" "Pokriva" "Rozek" "Rozgvezda" "Sedlevicz" "Slodkokostek" "Sneznogvezd" "Srebrnogryvski" "Szmaragdovicz" "Szuflokroski" "Tancovski" "Temnovicki" "Zelezny" "Zlatyblask"
	}
}

saddle_arabian = {
	mahri = { primary = "MAH" }
	aigiri = {}
	bida = {}
	aljan = {}
	jawharan = {}
	hafir = {}
	rummac = {}
}

treubh = {
	ialtag = {}
	sgeile = {}
	cruisc = {}
}

thestral = {
	yaras = {}
	mangoan = {}
	caual = {}
	zazacatl = {}
	acatl = {}
	analco = {} #I know it sounds funny, yes, but it literally means "beyond the sea"

	male_names = {
		Moctezuma Auitzotl Axayacatl Cuauhtemoc Cuitlahuac Huitzilihuitl Itzcoatl Ohimalpopoca Tizoc Motelchiuh Tehuetstiquitzin Tlacotzin Xochiquentzin Acamapichtli Chimalpopoca Tenoch Acacitli Axacaya Cacamatzin Eyahue Huicton Ilhuicamina Itzquautzin Mazatl Nopaltzin Olintecke Opochtli Quetzalmantzin Quimichetl Texcoyo Timas Tlacaelel Tlotzin Xiuhcozcatl Xocoyol Yaztachimal Zolton Ahcambal Huanitzin Panitzin Tariacuri Hiquingaje Hiripan Tangaxuan Tzitzipandaquare Zuangua Huitzimengari Tangaxoan Tzimtzincha Cuinierangari Bibejw Binech Bzojj Chs�axcejj Ch�alaxa Chonna Bicu Coqui Djoraxa Gw�ib�on� Gyazob Ipalnemoani Ji Jzi� Jituh L�xda�xo Lyebe Mbalaz Na Yel Naquichinisa Naxi�anguiiu Ngolbex An Rire Nisa Ro Pijje Tobi Rusiguunda V�eni Vezi� Zne Zni� Zyrx Gyejj Nguiiu
	}
	female_names = {
		Zyanya Nayeli Teiuc Teicuih Teyacapan Tepin Centehua Moyolehuani Yaotl Xipil Xitlali Xiuhcoatl Xochicotzin Xochiyotl Xoco Yayauhqui Yolihuani Yoloxochitl Yolyamanitzin Mixcoatl Necahual Nelli Ohtli Papa Patli
	}
	dynasty_names = {
		"Acatl" "Atl" "Calli" "Cipactli" "Coatl" "Coyotl" "Cozcaquauhtli" "Cuetzpalin" "Ehecatl" "Icpalli" 
		"Itz" "Itzcuintli" "Macehualli" "Malinalli" "Mazatl" "Miquiztli" "Nezahual" "Nochtli" "Ocelotl" 
		"Ollin" "Otli" "Ozomatli" "Pipiltin" "Pochtecatl" "Quauhtli" "Quauitl" "Quiautl" "Tecpatl" 
		"Tecuhtli" "Tepetl" "Tetl" "Tlantli" "Tochtli" "Toltecatl" "Xochitl"
	}
}

yak = {
	sarlag = {} #the name literally means "yak" in Mongolian, lol
	tuurai = {} 
	azarga = {}
}

changeling = {
	changeling_culture = {}
}


##Griffons
imperial = {
	high_imperial = {
	}
	
	low_imperial = {
	}
	
	female_names = {
		"Gabby" "Galena" "Gertie" "Gestal" "Gigi" "Gilda" "Gelda" "Gesta" "Gimme" "Giselle" "Gracie" "Greta" "Gretchen" "Griselda" 
		"Gala" "Gelana" "Gorraine" "Galsace" "Grace" "Gialiana" "Gaia" "Galie" "Grayfille" "Gashley" "Gamille" "Gyra" "Garje"
	}
	
	male_names = {
		"Gallus" "Garner" "Gaston" "Gavin" "Gerard" "Gordon" "Graff" "Gruff" "Gregor" "Grover" "Gunter" "Gustave" "Grand" "Guto" "Gildar"
		"Grinwings" "Gurphy" "Ghostbuck" "Gronzechase" "Geath" "Gobert" "Gierre" "Gideon" "Geon" "Grimes" "Gripoil" "Gorkuff" "Groille" "Grez"
		"Girwin" "Grance" "Garie" "Gerath"
	}
	dynasty_names = {
		"Lighttail" "Silvercrest" "Twilightstormer" "Redtalon" "Stormfeather" "Nighttalon" "Keegan" "Rainplume" "Daggerquill" "Netherquill" "Wrathbuck" "Bronzecrest" "Sharpwing" "Griffindor"
		"Sunwing" "Thundertalon" "Grimbeak" "Bronzebuck" "Hurley" "Mistwing" "Solarflare" "Crownfeather" "Marshstorm" "Ward" "Feytail" "Gladedive" "Jadestorm" "Shadowatcher" "Razortalon" "Madigan"
		"Duskwatcher" "Deathwing" "Daggerfeather" "Graydive"
	}
}

highgriff = {
	northern_grirhborn = {
	}
	
	southern_grirhborn = {
	}
	
	grivborn = {
	}
	
	female_names = {
		"Gabby" "Galena" "Gertie" "Gestal" "Gigi" "Gilda" "Gelda" "Gesta" "Gimme" "Giselle" "Gracie" "Greta" "Gretchen" "Griselda" 
		"Gala" "Gelana" "Gorraine" "Galsace" "Grace" "Gialiana" "Gaia" "Galie" "Grayfille" "Gashley" "Gamille" "Gyra" "Garje"
	}
	
	male_names = {
		"Gallus" "Garner" "Gaston" "Gavin" "Gerard" "Gordon" "Graff" "Gruff" "Gregor" "Grover" "Gunter" "Gustave" "Grand" "Guto" "Gildar"
		"Grinwings" "Gurphy" "Ghostbuck" "Gronzechase" "Geath" "Gobert" "Gierre" "Gideon" "Geon" "Grimes" "Gripoil" "Gorkuff" "Groille" "Grez"
		"Girwin" "Grance" "Garie" "Gerath"
	}
	dynasty_names = {
		"Lighttail" "Silvercrest" "Twilightstormer" "Redtalon" "Stormfeather" "Nighttalon" "Keegan" "Rainplume" "Daggerquill" "Netherquill" "Wrathbuck" "Bronzecrest" "Sharpwing" "Griffindor"
		"Sunwing" "Thundertalon" "Grimbeak" "Bronzebuck" "Hurley" "Mistwing" "Solarflare" "Crownfeather" "Marshstorm" "Ward" "Feytail" "Gladedive" "Jadestorm" "Shadowatcher" "Razortalon" "Madigan"
		"Duskwatcher" "Deathwing" "Daggerfeather" "Graydive"
	}
}

rygilvain = {
	ygrill = {
	}
	
	zonygriff = {
	}
	
	agridite = {
	}
	
	grifest = {
	}
	
	female_names = {
	}
	
	female_names = {
		"Gabby" "Galena" "Gertie" "Gestal" "Gigi" "Gilda" "Gelda" "Gesta" "Gimme" "Giselle" "Gracie" "Greta" "Gretchen" "Griselda" 
		"Gala" "Gelana" "Gorraine" "Galsace" "Grace" "Gialiana" "Gaia" "Galie" "Grayfille" "Gashley" "Gamille" "Gyra" "Garje"
	}
	
	male_names = {
		"Gallus" "Garner" "Gaston" "Gavin" "Gerard" "Gordon" "Graff" "Gruff" "Gregor" "Grover" "Gunter" "Gustave" "Grand" "Guto" "Gildar"
		"Grinwings" "Gurphy" "Ghostbuck" "Gronzechase" "Geath" "Gobert" "Gierre" "Gideon" "Geon" "Grimes" "Gripoil" "Gorkuff" "Groille" "Grez"
		"Girwin" "Grance" "Garie" "Gerath"
	}
	dynasty_names = {
		"Lighttail" "Silvercrest" "Twilightstormer" "Redtalon" "Stormfeather" "Nighttalon" "Keegan" "Rainplume" "Daggerquill" "Netherquill" "Wrathbuck" "Bronzecrest" "Sharpwing" "Griffindor"
		"Sunwing" "Thundertalon" "Grimbeak" "Bronzebuck" "Hurley" "Mistwing" "Solarflare" "Crownfeather" "Marshstorm" "Ward" "Feytail" "Gladedive" "Jadestorm" "Shadowatcher" "Razortalon" "Madigan"
		"Duskwatcher" "Deathwing" "Daggerfeather" "Graydive"
	}
}

skygriff = {
	frostback = {
	}
	
	steelbeak = {
	}
	
	greyson = {
	}
	
	bagrifer = {
	}
	
	griek = {
	}
	
	longclaw = {
	}
	
	female_names = {
		"Gabby" "Galena" "Gertie" "Gestal" "Gigi" "Gilda" "Gelda" "Gesta" "Gimme" "Giselle" "Gracie" "Greta" "Gretchen" "Griselda" 
		"Gala" "Gelana" "Gorraine" "Galsace" "Grace" "Gialiana" "Gaia" "Galie" "Grayfille" "Gashley" "Gamille" "Gyra" "Garje"
	}
	
	male_names = {
		"Gallus" "Garner" "Gaston" "Gavin" "Gerard" "Gordon" "Graff" "Gruff" "Gregor" "Grover" "Gunter" "Gustave" "Grand" "Guto" "Gildar"
		"Grinwings" "Gurphy" "Ghostbuck" "Gronzechase" "Geath" "Gobert" "Gierre" "Gideon" "Geon" "Grimes" "Gripoil" "Gorkuff" "Groille" "Grez"
		"Girwin" "Grance" "Garie" "Gerath"
	}
	dynasty_names = {
		"Lighttail" "Silvercrest" "Twilightstormer" "Redtalon" "Stormfeather" "Nighttalon" "Keegan" "Rainplume" "Daggerquill" "Netherquill" "Wrathbuck" "Bronzecrest" "Sharpwing" "Griffindor"
		"Sunwing" "Thundertalon" "Grimbeak" "Bronzebuck" "Hurley" "Mistwing" "Solarflare" "Crownfeather" "Marshstorm" "Ward" "Feytail" "Gladedive" "Jadestorm" "Shadowatcher" "Razortalon" "Madigan"
		"Duskwatcher" "Deathwing" "Daggerfeather" "Graydive"
	}
}

grislan = {
	gristalline = {
	}
	
	basgrone = {
	}
	
	kelagriff = {
	}
	
	rivgriff = {
	}
	
	sliteyes = {
	}
	
	riglass = {
	}
	
	female_names = {
		"Gabby" "Galena" "Gertie" "Gestal" "Gigi" "Gilda" "Gelda" "Gesta" "Gimme" "Giselle" "Gracie" "Greta" "Gretchen" "Griselda" 
		"Gala" "Gelana" "Gorraine" "Galsace" "Grace" "Gialiana" "Gaia" "Galie" "Grayfille" "Gashley" "Gamille" "Gyra" "Garje"
	}
	
	male_names = {
		"Gallus" "Garner" "Gaston" "Gavin" "Gerard" "Gordon" "Graff" "Gruff" "Gregor" "Grover" "Gunter" "Gustave" "Grand" "Guto" "Gildar"
		"Grinwings" "Gurphy" "Ghostbuck" "Gronzechase" "Geath" "Gobert" "Gierre" "Gideon" "Geon" "Grimes" "Gripoil" "Gorkuff" "Groille" "Grez"
		"Girwin" "Grance" "Garie" "Gerath"
	}
	dynasty_names = {
		"Lighttail" "Silvercrest" "Twilightstormer" "Redtalon" "Stormfeather" "Nighttalon" "Keegan" "Rainplume" "Daggerquill" "Netherquill" "Wrathbuck" "Bronzecrest" "Sharpwing" "Griffindor"
		"Sunwing" "Thundertalon" "Grimbeak" "Bronzebuck" "Hurley" "Mistwing" "Solarflare" "Crownfeather" "Marshstorm" "Ward" "Feytail" "Gladedive" "Jadestorm" "Shadowatcher" "Razortalon" "Madigan"
		"Duskwatcher" "Deathwing" "Daggerfeather" "Graydive"
	}
}

grion = {
	grilver = {
	}
	
	griffain = {
	}
	
	grishield = {
	}
	
	gritaur = {
	}
	
	grizail = {
	}
	
	female_names = {
		"Gabby" "Galena" "Gertie" "Gestal" "Gigi" "Gilda" "Gelda" "Gesta" "Gimme" "Giselle" "Gracie" "Greta" "Gretchen" "Griselda" 
		"Gala" "Gelana" "Gorraine" "Galsace" "Grace" "Gialiana" "Gaia" "Galie" "Grayfille" "Gashley" "Gamille" "Gyra" "Garje"
	}
	
	male_names = {
		"Gallus" "Garner" "Gaston" "Gavin" "Gerard" "Gordon" "Graff" "Gruff" "Gregor" "Grover" "Gunter" "Gustave" "Grand" "Guto" "Gildar"
		"Grinwings" "Gurphy" "Ghostbuck" "Gronzechase" "Geath" "Gobert" "Gierre" "Gideon" "Geon" "Grimes" "Gripoil" "Gorkuff" "Groille" "Grez"
		"Girwin" "Grance" "Garie" "Gerath"
	}
	dynasty_names = {
		"Lighttail" "Silvercrest" "Twilightstormer" "Redtalon" "Stormfeather" "Nighttalon" "Keegan" "Rainplume" "Daggerquill" "Netherquill" "Wrathbuck" "Bronzecrest" "Sharpwing" "Griffindor"
		"Sunwing" "Thundertalon" "Grimbeak" "Bronzebuck" "Hurley" "Mistwing" "Solarflare" "Crownfeather" "Marshstorm" "Ward" "Feytail" "Gladedive" "Jadestorm" "Shadowatcher" "Razortalon" "Madigan"
		"Duskwatcher" "Deathwing" "Daggerfeather" "Graydive"
	}
}
