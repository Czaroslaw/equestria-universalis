add_core = I71
owner = I71
controller = I71
culture = gritaur
religion = claws
hre = no
base_tax = 2 
base_production = 3
trade_goods = iron
base_manpower = 2
is_city = yes
discovered_by = eastern
discovered_by = western
discovered_by = muslim
discovered_by = ottoman