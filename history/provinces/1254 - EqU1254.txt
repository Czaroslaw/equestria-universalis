add_core = G69
owner = G69
controller = G69
culture = ygrill
religion = covert
hre = no
base_tax = 2 
base_production = 3
trade_goods = iron
base_manpower = 2
is_city = yes
discovered_by = eastern
discovered_by = western
discovered_by = muslim
discovered_by = ottoman