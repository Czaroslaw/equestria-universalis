add_core = H16
owner = H16
controller = H16
culture = bagrifer
religion = patagialis
hre = no
base_tax = 2 
base_production = 3
trade_goods = iron
base_manpower = 2
is_city = yes
discovered_by = eastern
discovered_by = western
discovered_by = muslim
discovered_by = ottoman