# No previous file for EqU827
# No previous file for EqU826
add_core = G01
owner = G01
controller = G01
culture = high_imperial
religion = alula
hre = no
base_tax = 2 
base_production = 3
trade_goods = iron
base_manpower = 2
is_city = yes
discovered_by = eastern
discovered_by = western
discovered_by = muslim
discovered_by = ottoman