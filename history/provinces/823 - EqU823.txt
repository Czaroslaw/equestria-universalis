# No previous file for EqU823
add_core = G00
owner = G00
controller = G00
culture = high_imperial
religion = alula
hre = no
base_tax = 2 
base_production = 3
trade_goods = iron
base_manpower = 2
is_city = yes
discovered_by = eastern
discovered_by = western
discovered_by = muslim
discovered_by = ottoman