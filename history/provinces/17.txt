add_core = CAN
owner = CAN
controller = CAN
culture = upper_ponish
religion = harmonic
hre = yes
base_tax = 1
base_production = 1
trade_goods = grain
base_manpower = 1
is_city = yes
discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = ottoman

hre = yes
