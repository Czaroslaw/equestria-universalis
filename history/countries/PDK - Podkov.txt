government = monarchy
add_government_reform = autocracy_reform
technology_group = western
religion = harmonic
primary_culture = lovanian
add_accepted_culture = sedlan
government_rank = 1
capital = 758

581.1.1 = {
	monarch = {
		name = "Comet"
		dynasty = "von Kuzich"
		birth_date = 551.6.12
		adm = 4
		dip = 6
		mil = 4
	}
	heir = {
		name = "Astral"
		dynasty = "von Kuzich"
		birth_date = 575.12.1
		adm = 3
		dip = 2
		mil = 6
	}
}
