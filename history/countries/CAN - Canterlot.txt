government = monarchy
add_government_reform = equestrian_princedom_reform
technology_group = western
religion = harmonic
primary_culture = upper_ponish
government_rank = 1
historical_friend = CRY
capital = 1 # Canterlot

581.1.1 = {
	add_country_modifier = { name = celestias_reign duration = -1 }

	monarch = {
		name = "Celestia"
		dynasty = "of Canterlot"
		birth_date = 10.1.1
		adm = 3
		dip = 3
		mil = 1
		female = yes
		}
		
	clear_scripted_personalities = yes
	add_ruler_personality = immortal_personality
	add_ruler_personality = kind_hearted_personality
	add_ruler_personality = well_advised_personality
	}