#Canterlot-Crystal Empire
royal_marriage = {
	first = CAN
	second = CRY
}

#Crystal Empire-Trotsylvania
march = {
	first = CRY
	second = TRO
}

#Prancia-Kohlania
guarantee = {
	first = PRA
	second = KOH
}
